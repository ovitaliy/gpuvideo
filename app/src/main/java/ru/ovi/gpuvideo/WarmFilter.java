package ru.ovi.gpuvideo;

import ru.ovi.gpuvideolibrary.filters.WhiteBalanceFilter;

/**
 * Created by ovi on 3/2/16.
 */
public class WarmFilter extends WhiteBalanceFilter {

    public WarmFilter() {
        super(12000, 0);
    }
}
