package ru.ovi.gpuvideo;

import ru.ovi.gpuvideolibrary.filters.GaussianBlurFilter;
import ru.ovi.gpuvideolibrary.filters.IFilter;
import ru.ovi.gpuvideolibrary.filters.IGroupFilters;
import ru.ovi.gpuvideolibrary.filters.ImageToonFilter;

/**
 * Created by ovi on 3/4/16.
 */
public class ComicFilter implements IGroupFilters {


    private float mWidth;
    private float mHeight;

    public ComicFilter(float width, float height) {
        mWidth = width;
        mHeight = height;
    }


    @Override
    public IFilter[] getFilters() {
        return new IFilter[]{
                new GaussianBlurFilter(0.5f, mWidth, mHeight),
                new GaussianBlurFilter(0.5f, mWidth, mHeight),
                new ImageToonFilter(1f, mWidth, mHeight)
        };
    }
}
