package ru.ovi.gpuvideo;

import ru.ovi.gpuvideolibrary.filters.IFilter;
import ru.ovi.gpuvideolibrary.filters.SobelEdgeDetectionFilter;

/**
 * Created by ovi on 3/2/16.
 */
public class PopartFilter extends SobelEdgeDetectionFilter {
    public PopartFilter(float size, float outputWidth, float outputHeight) {
        super(size, outputWidth, outputHeight);
    }

    @Override
    public IFilter[] getFilters() {
        return new IFilter[]{
                mTexture3x3SamplingFilter,
                new GrayFilter()
        };
    }
}
