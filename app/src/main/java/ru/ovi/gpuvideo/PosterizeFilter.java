package ru.ovi.gpuvideo;

import ru.ovi.gpuvideolibrary.filters.BaseFilter;

/**
 * Created by ovi on 3/2/16.
 */
public class PosterizeFilter extends BaseFilter {

    private float mColorLevels;

    public PosterizeFilter(float colorLevels) {
        mColorLevels = colorLevels;
    }

    @Override
    public String getFragmentShader() {
        return String.format("#extension GL_OES_EGL_image_external : require\n" +
                        "varying highp vec2 vTextureCoord;\n" +
                        "\n" +
                        "uniform samplerExternalOES sTexture;\n" +
                        "highp float colorLevels = %s;\n" +
                        "\n" +
                        "void main()\n" +
                        "{\n" +
                        "   highp vec4 textureColor = texture2D(sTexture, vTextureCoord);\n" +
                        "   \n" +
                        "   gl_FragColor = floor((textureColor * colorLevels) + vec4(0.5)) / colorLevels;\n" +
                        "}",
                floatToString(mColorLevels))
                ;
    }
}
