package ru.ovi.gpuvideo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.VideoView;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ru.ovi.gpuvideolibrary.VideoProcessor;
import ru.ovi.gpuvideolibrary.filters.IFilter;
import ru.ovi.gpuvideolibrary.filters.IGroupFilters;

public class MainActivity extends AppCompatActivity {


    private static final File OUTPUT_FILENAME_DIR = new File(Environment.getExternalStorageDirectory(), "tt");
    // private static File inputFile = new File(OUTPUT_FILENAME_DIR, "video_tmp_2.mp4");
    private static File inputFile = new File(OUTPUT_FILENAME_DIR, "input.mp4");
    private static File outputFile = new File(OUTPUT_FILENAME_DIR, "output.mp4");
    private static File output2File = new File(OUTPUT_FILENAME_DIR, "output3.mp4");

    private static final String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private Object[] mIFilters = new Object[]{

            new CheFilter(480, 480),

            new PixelFilter(480, 480),

            new GrittyFilter(this, 480, 480),

            new ComicFilter(480, 480),
            new CinematicFilter(0.3f, 480, 480),

            new CoolFilter(),


            new PopartFilter(1, 480, 480),
            new BulgeFilter(480, 480),
            new WarmFilter(),

            new SepiaFilter(),
            new GrayFilter(),
            new OilFilter(),
            new PosterizeFilter(7.95f),
            new PopartFilter(1, 480, 480)
    };

    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mVideoView = (VideoView) findViewById(R.id.video);


        Spinner spinner = ((Spinner) findViewById(R.id.spinner));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Object o = mIFilters[position];

                final String outputFileName = new File(OUTPUT_FILENAME_DIR, o.getClass().getSimpleName() + ".mp4").getAbsolutePath();

                IFilter[] iFilters;
                if (o instanceof IFilter) {
                    iFilters = new IFilter[]{(IFilter) o};
                } else {
                    iFilters = ((IGroupFilters) o).getFilters();
                }

                runGroup(iFilters, outputFileName);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> filterNames = new ArrayList<>();
        for (Object o : mIFilters) {
            filterNames.add(o.getClass().getSimpleName());
        }
        spinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, filterNames));


        checkPermissionAndRun();
    }

    private void checkPermissionAndRun() {
        if (!checkPermissions())
            return;


        if (!OUTPUT_FILENAME_DIR.exists()) {
            OUTPUT_FILENAME_DIR.mkdirs();
        }

        try {
            InputStream in = getAssets().open("input.mp4");
            ;
            OutputStream out = new FileOutputStream(inputFile);
            IOUtils.copyLarge(in, out);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private boolean checkPermissions() {
        List<String> requiredPermissions = new ArrayList<>();
        for (String permission : PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                requiredPermissions.add(permission);
            }
        }

        if (requiredPermissions.size() == 0) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this,
                    requiredPermissions.toArray(new String[1]),
                    10);

            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        checkPermissionAndRun();
    }

    private void runGroup(IFilter[] filters, final String outputFileName) {
        final long start = System.currentTimeMillis();

        Executor executor = Executors.newSingleThreadExecutor();

        int n = filters.length;

        String[] tmpNames = new String[n + 1];
        tmpNames[0] = inputFile.getAbsolutePath();
        for (int i = 0; i < n - 1; i++) {
            tmpNames[i + 1] = new File(OUTPUT_FILENAME_DIR, (i + 1) + ".mp4").getAbsolutePath();
        }
        tmpNames[n] = outputFileName;

        for (int i = 0; i < n; i++) {
            VideoProcessor.Builder builder = new VideoProcessor.Builder()
                    .setInputFile(tmpNames[i])
                    .setOutputFile(tmpNames[i + 1])
                    .setFilter(filters[i])
                    .setSize(480, 480);

            if (i == n - 1) {
                builder.setOnVideoProcessListener(new VideoProcessor.OnVideoProcessListener() {
                    @Override
                    public void onVideoProcessCompleted() {
                        long done = System.currentTimeMillis() - start;
                        Log.e("MainActivity", "done at:" + done);

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                mVideoView.setVideoPath("file://" + outputFileName);
                                mVideoView.start();
                            }
                        });

                    }
                });
            }

            Runnable runnable = builder.build().getProcessorRunnable();
            executor.execute(runnable);
        }


    }
}
