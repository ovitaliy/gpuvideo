package ru.ovi.gpuvideo;

import android.content.Context;

import ru.ovi.gpuvideolibrary.filters.IFilter;
import ru.ovi.gpuvideolibrary.filters.IGroupFilters;
import ru.ovi.gpuvideolibrary.filters.LookupFilter;
import ru.ovi.gpuvideolibrary.filters.SharpenFilter;

/**
 * Created by ovi on 3/3/16.
 */
public class GrittyFilter implements IGroupFilters {

    private Context mContext;

    private static final float mSharpness = 0.8f;
    private float mWidth;
    private float mHeight;

    public GrittyFilter(Context context, final float width, final float height) {
        mContext = context;
        mWidth = width;
        mHeight = height;
    }

    @Override
    public IFilter[] getFilters() {
        return new IFilter[]{
                new SharpenFilter(mSharpness, mWidth, mHeight),
                new LookupFilter(mContext)
        };
    }
}
