package ru.ovi.gpuvideo;

import ru.ovi.gpuvideolibrary.filters.BaseFilter;

/**
 * Created by ovi on 3/3/16.
 */
public class CoolFilter extends BaseFilter {


    @Override
    public String getFragmentShader() {
        return
                "#extension GL_OES_EGL_image_external : require\n" +
                        "precision highp float;\n" +

                        "varying vec2 vTextureCoord;\n" +

                        "uniform samplerExternalOES sTexture;\n" +

                        "const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);\n" +

                      //  "const mediump vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);\n" +


                        "vec4 RGB(vec4 textureColor, float r, float g, float b){\n" +
                        "   return vec4(textureColor.r * r, textureColor.g * g, textureColor.b * b, 1.0);\n" +
                        "}\n" +

                        "vec4 saturation(vec4 textureColor, float saturation){\n" +
                        "   mediump vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);\n" +
                        "   lowp float luminance = dot(textureColor.rgb, luminanceWeighting);\n" +
                        "   lowp vec3 greyScaleColor = vec3(luminance);\n" +
                        "   return vec4(mix(greyScaleColor, textureColor.rgb, saturation), textureColor.w);\n" +
                        "}\n" +

                        "vec4 contrast(vec4 textureColor, float contrast){\n" +
                        "   return vec4(((textureColor.rgb - vec3(0.5)) * contrast + vec3(0.5)), textureColor.w);\n" +
                        "}\n" +

                        "vec4 brightness(vec4 textureColor, float brightness){\n" +
                        "   return vec4((textureColor.rgb + vec3(brightness)), textureColor.w);\n" +
                        "}\n" +

                        "vec4 HighlightShadow(vec4 source, float shadows, float highlights){\n" +
                        "   mediump vec3 luminanceWeightingHS = vec3(0.3, 0.3, 0.3);\n" +

                        " 	mediump float luminance = dot(source.rgb, luminanceWeightingHS);\n" +
                        " \n" +
                        " 	mediump float shadow = clamp((pow(luminance, 1.0/(shadows+1.0)) + (-0.76)*pow(luminance, 2.0/(shadows+1.0))) - luminance, 0.0, 1.0);\n" +
                        " 	mediump float highlight = clamp((1.0 - (pow(1.0-luminance, 1.0/(2.0-highlights)) + (-0.8)*pow(1.0-luminance, 2.0/(2.0-highlights)))) - luminance, -1.0, 0.0);\n" +
                        " 	lowp vec3 result = vec3(0.0, 0.0, 0.0) + ((luminance + shadow + highlight) - 0.0) * ((source.rgb - vec3(0.0, 0.0, 0.0))/(luminance - 0.0));\n" +

                        "   return vec4(result.rgb, source.a);\n" +
                        "}\n" +


                        "vec4 hue(vec4 color, float hueAdjust){\n" +
                        "    highp vec4 kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);\n" +
                        "    highp vec4 kRGBToI = vec4 (0.595716, -0.274453, -0.321263, 0.0);\n" +
                        "    highp vec4 kRGBToQ = vec4 (0.211456, -0.522591, 0.31135, 0.0);\n" +
                        "\n" +
                        "    highp vec4 kYIQToR = vec4 (1.0, 0.9563, 0.6210, 0.0);\n" +
                        "    highp vec4 kYIQToG = vec4 (1.0, -0.2721, -0.6474, 0.0);\n" +
                        "    highp vec4 kYIQToB = vec4 (1.0, -1.1070, 1.7046, 0.0);\n" +
                        "    // Convert to YIQ\n" +
                        "    highp float YPrime = dot (color, kRGBToYPrime);\n" +
                        "    highp float I = dot (color, kRGBToI);\n" +
                        "    highp float Q = dot (color, kRGBToQ);\n" +
                        "\n" +
                        "    // Calculate the hue and chroma\n" +
                        "    highp float hue = atan (Q, I);\n" +
                        "    highp float chroma = sqrt (I * I + Q * Q);\n" +
                        "\n" +
                        "    // Make the user's adjustments\n" +
                        "    hue += (-hueAdjust); //why negative rotation?\n" +
                        "\n" +
                        "    // Convert back to YIQ\n" +
                        "    Q = chroma * sin (hue);\n" +
                        "    I = chroma * cos (hue);\n" +
                        "\n" +
                        "    // Convert back to RGB\n" +
                        "    highp vec4 yIQ = vec4 (YPrime, I, Q, 0.0);\n" +
                        "    color.r = dot (yIQ, kYIQToR);\n" +
                        "    color.g = dot (yIQ, kYIQToG);\n" +
                        "    color.b = dot (yIQ, kYIQToB);\n" +
                        "\n" +
                        "    // Save the result\n" +
                        "    return color;\n" +

                        "}\n" +

                        "void main()\n" +
                        "{\n" +
                        "  lowp vec4 textureColor = texture2D(sTexture, vTextureCoord);\n" +


                        "  textureColor =  hue(textureColor, 193.0);\n" +
                        "  textureColor =  RGB(textureColor, 1.02, 1.045, 1.2);\n" +
                        "  textureColor =  contrast(textureColor, 1.5);\n" +

                        /*"  textureColor =  saturation(textureColor, 1.5);\n" +
                        "  textureColor =  RGB(textureColor, 1.0, 1.4, 1.2);\n" +
                        "  textureColor =  contrast(textureColor, 1.4);\n" +
                        "  textureColor =  brightness(textureColor, 0.3);\n" +
                        "  textureColor =  HighlightShadow(textureColor, 0.9, 0.4);\n" +*/
                        "\n" +
                        "  gl_FragColor =  textureColor;\n" +


                        "}"


                ;
    }
}
