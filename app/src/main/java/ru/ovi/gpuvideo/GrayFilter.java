package ru.ovi.gpuvideo;

import ru.ovi.gpuvideolibrary.filters.BaseFilter;

/**
 * Created by ovi on 3/1/16.
 */
public class GrayFilter extends BaseFilter {

    @Override
    public String getFragmentShader() {
        return "#extension GL_OES_EGL_image_external : require\n" +
                "precision highp float;\n" +
                "\n" +
                "varying vec2 vTextureCoord;\n" +
                "\n" +
                "uniform samplerExternalOES sTexture;\n" +
                "\n" +
                "const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);\n" +
                "\n" +
                "void main()\n" +
                "{\n" +
                "  lowp vec4 textureColor = texture2D(sTexture, vTextureCoord);\n" +
                "  float luminance = dot(textureColor.rgb, W);\n" +
                "\n" +
                "  gl_FragColor = vec4(vec3(luminance), textureColor.a);\n" +
                "}";
    }
}
