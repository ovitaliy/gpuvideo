package ru.ovi.gpuvideo;

import ru.ovi.gpuvideolibrary.filters.BaseFilter;

/**
 * Created by ovi on 3/3/16.
 */
public class BulgeFilter extends BaseFilter {

    private float mAspectRatio = 1;

    public BulgeFilter(float width, float height) {
        mAspectRatio = width / height;
    }

    @Override
    public String getFragmentShader() {
        return String.format(
                "#extension GL_OES_EGL_image_external : require\n" +
                        "varying highp vec2 vTextureCoord;\n" +
                        "\n" +
                        "uniform samplerExternalOES sTexture;\n" +
                        "\n" +
                        "highp float aspectRatio = %s;\n" +
                        "highp vec2 center = vec2(%s);\n" +
                        "highp float radius = %s;\n" +
                        "highp float scale = %s;\n" +
                        "\n" +
                        "void main()\n" +
                        "{\n" +
                        "highp vec2 textureCoordinateToUse = vec2(vTextureCoord.x, (vTextureCoord.y * aspectRatio + 0.5 - 0.5 * aspectRatio));\n" +
                        "highp float dist = distance(center, textureCoordinateToUse);\n" +
                        "textureCoordinateToUse = vTextureCoord;\n" +
                        "\n" +
                        "if (dist < radius)\n" +
                        "{\n" +
                        "textureCoordinateToUse -= center;\n" +
                        "highp float percent = 1.0 - ((radius - dist) / radius) * scale;\n" +
                        "percent = percent * percent;\n" +
                        "\n" +
                        "textureCoordinateToUse = textureCoordinateToUse * percent;\n" +
                        "textureCoordinateToUse += center;\n" +
                        "}\n" +
                        "\n" +
                        "gl_FragColor = texture2D(sTexture, textureCoordinateToUse );    \n" +
                        "}\n",

                /*aspectRatio*/ BaseFilter.floatToString(mAspectRatio),
                /*center*/ "0.5, 0.5",
                /*radius*/ BaseFilter.floatToString(1.0f),
                /*scale*/ BaseFilter.floatToString(.5f)
        );
    }
}
