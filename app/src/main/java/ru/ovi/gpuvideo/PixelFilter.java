package ru.ovi.gpuvideo;

import ru.ovi.gpuvideolibrary.filters.BaseFilter;

/**
 * Created by ovi on 3/4/16.
 */
public class PixelFilter extends BaseFilter {


    private float mImageWidthFactorLocation;
    private float mImageHeightFactorLocation;

    public PixelFilter(final float width, final float height) {
        mImageWidthFactorLocation = 1.0f / width;
        mImageHeightFactorLocation = 1.0f / height;
    }

    @Override
    public String getFragmentShader() {
        return
                "#extension GL_OES_EGL_image_external : require\n" +
                        "precision mediump float;\n" +      // highp here doesn't seem to matter
                        "varying vec2 vTextureCoord;\n" +
                        "uniform samplerExternalOES sTexture;\n" +

                        "float imageWidthFactor = " + floatToString(mImageWidthFactorLocation) + ";\n" +
                        "float imageHeightFactor = " + floatToString(mImageHeightFactorLocation) + ";\n" +
                        "float pixel = " + floatToString(6) + ";\n" +

                        "void main()\n" +
                        "{\n" +
                        "  vec2 uv  = vTextureCoord.xy;\n" +
                        "  float dx = pixel * imageWidthFactor;\n" +
                        "  float dy = pixel * imageHeightFactor;\n" +
                        "  vec2 coord = vec2(dx * floor(uv.x / dx), dy * floor(uv.y / dy));\n" +
                        "  vec3 tc = texture2D(sTexture, coord).xyz;\n" +
                        "  gl_FragColor = vec4(tc, 1.0);\n" +
                        "}";

    }
}
