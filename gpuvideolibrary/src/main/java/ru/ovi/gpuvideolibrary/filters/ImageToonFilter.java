package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/4/16.
 */
public class ImageToonFilter extends Texture3x3SamplingFilter{


    public ImageToonFilter(float size, float outputWidth, float outputHeight) {
        super(size, outputWidth, outputHeight);
    }

    @Override
    public String getFragmentShader() {
        return "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +

                "\n" +
                "varying vec2 aTextureCoord;\n" +
                "varying vec2 leftTextureCoordinate;\n" +
                "varying vec2 rightTextureCoordinate;\n" +
                "\n" +
                "varying vec2 topTextureCoordinate;\n" +
                "varying vec2 topLeftTextureCoordinate;\n" +
                "varying vec2 topRightTextureCoordinate;\n" +
                "\n" +
                "varying vec2 bottomTextureCoordinate;\n" +
                "varying vec2 bottomLeftTextureCoordinate;\n" +
                "varying vec2 bottomRightTextureCoordinate;\n" +
                "\n" +
                "uniform samplerExternalOES sTexture;\n" +
                "\n" +
                "highp float threshold = "+floatToString(0.15f)+";\n" +
                "highp float quantizationLevels = "+floatToString(10)+";\n" +
                "\n" +
                "const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);\n" +
                "\n" +
                "void main()\n" +
                "{\n" +
                "vec4 textureColor = texture2D(sTexture, aTextureCoord);\n" +
                "\n" +
                "float bottomLeftIntensity = texture2D(sTexture, bottomLeftTextureCoordinate).r;\n" +
                "float topRightIntensity = texture2D(sTexture, topRightTextureCoordinate).r;\n" +
                "float topLeftIntensity = texture2D(sTexture, topLeftTextureCoordinate).r;\n" +
                "float bottomRightIntensity = texture2D(sTexture, bottomRightTextureCoordinate).r;\n" +
                "float leftIntensity = texture2D(sTexture, leftTextureCoordinate).r;\n" +
                "float rightIntensity = texture2D(sTexture, rightTextureCoordinate).r;\n" +
                "float bottomIntensity = texture2D(sTexture, bottomTextureCoordinate).r;\n" +
                "float topIntensity = texture2D(sTexture, topTextureCoordinate).r;\n" +
                "float h = -topLeftIntensity - 2.0 * topIntensity - topRightIntensity + bottomLeftIntensity + 2.0 * bottomIntensity + bottomRightIntensity;\n" +
                "float v = -bottomLeftIntensity - 2.0 * leftIntensity - topLeftIntensity + bottomRightIntensity + 2.0 * rightIntensity + topRightIntensity;\n" +
                "\n" +
                "float mag = length(vec2(h, v));\n" +
                "\n" +
                "vec3 posterizedImageColor = floor((textureColor.rgb * quantizationLevels) + 0.5) / quantizationLevels;\n" +
                "\n" +
                "float thresholdTest = 1.0 - step(threshold, mag);\n" +
                "\n" +
                "gl_FragColor = vec4(posterizedImageColor * thresholdTest, textureColor.a);\n" +
                "}\n";
    }
}
