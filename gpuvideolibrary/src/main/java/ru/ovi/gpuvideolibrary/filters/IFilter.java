package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/1/16.
 */
public interface IFilter {

    String getTitle();

    String getVertexShader();

    String getFragmentShader();

    void init(int program);

    void onDrawArraysPre();

}
