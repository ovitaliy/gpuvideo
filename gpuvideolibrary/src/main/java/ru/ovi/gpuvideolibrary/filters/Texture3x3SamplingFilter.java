package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/2/16.
 */
public abstract class Texture3x3SamplingFilter extends BaseFilter {

    private float mTexelWidth;
    private float mTexelHeight;

    public Texture3x3SamplingFilter(final float size, final float outputWidth, final float outputHeight) {
        mTexelWidth = size / outputWidth;
        mTexelHeight = size / outputHeight;
    }

    @Override
    public String getVertexShader() {
        return String.format(
                "uniform mat4 uMVPMatrix;\n" +
                        "uniform mat4 uSTMatrix;\n" +
                        "attribute vec4 aPosition;\n" +
                        "attribute vec4 aTextureCoord;\n" +

                        "\n" +
                        "highp float texelWidth = %s; \n" +
                        "highp float texelHeight = %s; \n" +
                        "\n" +
                        "varying vec2 textureCoordinate;\n" +
                        "varying vec2 leftTextureCoordinate;\n" +
                        "varying vec2 rightTextureCoordinate;\n" +
                        "\n" +
                        "varying vec2 topTextureCoordinate;\n" +
                        "varying vec2 topLeftTextureCoordinate;\n" +
                        "varying vec2 topRightTextureCoordinate;\n" +
                        "\n" +
                        "varying vec2 bottomTextureCoordinate;\n" +
                        "varying vec2 bottomLeftTextureCoordinate;\n" +
                        "varying vec2 bottomRightTextureCoordinate;\n" +
                        "\n" +
                        "void main()\n" +
                        "{\n" +
                        "     gl_Position = uMVPMatrix * aPosition;\n" +
                        "\n" +
                        "    vec2 widthStep = vec2(texelWidth, 0.0);\n" +
                        "    vec2 heightStep = vec2(0.0, texelHeight);\n" +
                        "    vec2 widthHeightStep = vec2(texelWidth, texelHeight);\n" +
                        "    vec2 widthNegativeHeightStep = vec2(texelWidth, -texelHeight);\n" +
                        "\n" +
                        "    textureCoordinate = (uSTMatrix * aTextureCoord).xy;\n" +
                        "    leftTextureCoordinate = (uSTMatrix * aTextureCoord).xy - widthStep;\n" +
                        "    rightTextureCoordinate = (uSTMatrix * aTextureCoord).xy + widthStep;\n" +
                        "\n" +
                        "    topTextureCoordinate = (uSTMatrix * aTextureCoord).xy - heightStep;\n" +
                        "    topLeftTextureCoordinate = (uSTMatrix * aTextureCoord).xy - widthHeightStep;\n" +
                        "    topRightTextureCoordinate = (uSTMatrix * aTextureCoord).xy + widthNegativeHeightStep;\n" +
                        "\n" +
                        "    bottomTextureCoordinate = (uSTMatrix * aTextureCoord).xy + heightStep;\n" +
                        "    bottomLeftTextureCoordinate = (uSTMatrix * aTextureCoord).xy - widthNegativeHeightStep;\n" +
                        "    bottomRightTextureCoordinate = (uSTMatrix * aTextureCoord).xy + widthHeightStep;\n" +
                        "}",
                floatToString(mTexelWidth),
                floatToString(mTexelHeight)

        );
    }


    public abstract String getFragmentShader();

}
