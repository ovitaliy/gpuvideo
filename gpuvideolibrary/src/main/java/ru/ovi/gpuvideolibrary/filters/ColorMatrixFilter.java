package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/2/16.
 */
public class ColorMatrixFilter extends BaseFilter {

    private float mIntensity;
    private float[] mMatrix;

    public ColorMatrixFilter(float intensity, float[] matrix) {
        mIntensity = intensity;
        mMatrix = matrix;
        if (matrix == null || matrix.length != 16)
            throw new RuntimeException("maxtix is null or matrix lenght not equals 16");
    }

    @Override
    public String getFragmentShader() {
        return String.format(
                "#extension GL_OES_EGL_image_external : require\n" +
                        "varying vec2 vTextureCoord;\n" +
                        "uniform samplerExternalOES sTexture;\n" +
                        "lowp mat4 colorMatrix = %s;\n" +
                        "lowp float intensity = %s;\n" +
                        "\n" +
                        "void main()\n" +
                        "{\n" +
                        "    lowp vec4 textureColor = texture2D(sTexture, vTextureCoord);\n" +
                        "    lowp vec4 outputColor = textureColor * colorMatrix;\n" +
                        "    \n" +
                        "    gl_FragColor = (intensity * outputColor) + ((1.0 - intensity) * textureColor);\n" +
                        "}",
                matrixToString(mMatrix),
                floatToString(mIntensity)

        );
    }

    static String matrixToString(float[] matrix) {
        String result = "mat4(";
        int i = 0;
        for (; i < matrix.length - 1; i++) {
            result += floatToString(matrix[i]) + ",";
        }
        result += floatToString(matrix[i]) + ")";
        return result;
    }


}
