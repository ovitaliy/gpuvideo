package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/1/16.
 */
public class BaseFilter implements IFilter {


    @Override
    public String getTitle() {
        return getClass().getSimpleName();
    }

    @Override
    public String getVertexShader() {
        return "uniform mat4 uMVPMatrix;\n" +
                "uniform mat4 uSTMatrix;\n" +
                "attribute vec4 aPosition;\n" +
                "attribute vec4 aTextureCoord;\n" +
                "varying vec2 vTextureCoord;\n" +
                "void main() {\n" +
                "  gl_Position = uMVPMatrix * aPosition;\n" +
                "  vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n" +
                "}\n";
    }

    @Override
    public String getFragmentShader() {
        return "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +      // highp here doesn't seem to matter
                "varying vec2 vTextureCoord;\n" +
                "uniform samplerExternalOES sTexture;\n" +
                "void main() {\n" +
                "  gl_FragColor = texture2D(sTexture, vTextureCoord);\n" +
                "}\n";
    }


    public static String floatToString(float value) {
        String result = String.valueOf(value);
        result = result.replace(",", ".");
        return result;
    }

    public void init(int program){}

    public void onDrawArraysPre() {}
}
