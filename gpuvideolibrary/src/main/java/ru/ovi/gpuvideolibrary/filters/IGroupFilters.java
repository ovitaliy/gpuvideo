package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/2/16.
 */
public interface IGroupFilters {

    IFilter[] getFilters();

}
