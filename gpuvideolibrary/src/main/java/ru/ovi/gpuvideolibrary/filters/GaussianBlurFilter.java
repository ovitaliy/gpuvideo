package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/1/16.
 */
public class GaussianBlurFilter extends BaseFilter {

    private float mBlurSize;
    private float mWidth;
    private float mHeight;

    public GaussianBlurFilter(float blurSize, float width, float height) {
        mBlurSize = blurSize;
        mWidth = width;
        mHeight = height;
    }

    @Override
    public String getVertexShader() {

        String shader = (
                "uniform mat4 uMVPMatrix;\n" +
                        "uniform mat4 uSTMatrix;\n" +
                        "attribute vec4 aPosition;\n" +
                        "attribute vec4 aTextureCoord;\n" +

                        "\n" +

                        "const int GAUSSIAN_SAMPLES = 9;\n" +
                        "\n" +
                        "float texelWidthOffset = " + floatToString(mBlurSize / mWidth) + ";\n" +
                        "float texelHeightOffset = " + floatToString(mBlurSize / mHeight) + ";\n" +

                        "\n" +

                        "varying vec2 vTextureCoord;\n" +
                        "varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +
                        "\n" +
                        "void main()\n" +
                        "{\n" +
                        "	gl_Position = uMVPMatrix * aPosition;\n" +
                        "	vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n" +
                        "	\n" +
                        "	// Calculate the positions for the blur\n" +
                        "	int multiplier = 0;\n" +
                        "	vec2 blurStep;\n" +
                        "   vec2 singleStepOffset = vec2(texelHeightOffset, texelWidthOffset);\n" +
                        "    \n" +
                        "	for (int i = 0; i < GAUSSIAN_SAMPLES; i++)\n" +
                        "   {\n" +
                        "		multiplier = (i - ((GAUSSIAN_SAMPLES - 1) / 2));\n" +
                        "       // Blur in x (horizontal)\n" +
                        "       blurStep = float(multiplier) * singleStepOffset;\n" +
                        "		blurCoordinates[i] = aTextureCoord.xy + blurStep;\n" +
                        "	}\n" +
                        "}\n"
        );


        return shader;
    }


    @Override
    public String getFragmentShader() {
        return "#extension GL_OES_EGL_image_external : require\n" +
                "uniform samplerExternalOES sTexture;\n" +
                "\n" +
                "const int GAUSSIAN_SAMPLES = 9;\n" +
                "\n" +
                "varying vec2 vTextureCoord;\n" +
                "varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];\n" +
                "\n" +
                "void main()\n" +
                "{\n" +
                "	vec3 sum = vec3(0.0);\n" +
                "   vec4 fragColor=texture2D(sTexture,vTextureCoord);\n" +
                "	\n" +
                "    sum += texture2D(sTexture, blurCoordinates[0]).rgb * 0.05;\n" +
                "    sum += texture2D(sTexture, blurCoordinates[1]).rgb * 0.09;\n" +
                "    sum += texture2D(sTexture, blurCoordinates[2]).rgb * 0.12;\n" +
                "    sum += texture2D(sTexture, blurCoordinates[3]).rgb * 0.15;\n" +
                "    sum += texture2D(sTexture, blurCoordinates[4]).rgb * 0.18;\n" +
                "    sum += texture2D(sTexture, blurCoordinates[5]).rgb * 0.15;\n" +
                "    sum += texture2D(sTexture, blurCoordinates[6]).rgb * 0.12;\n" +
                "    sum += texture2D(sTexture, blurCoordinates[7]).rgb * 0.09;\n" +
                "    sum += texture2D(sTexture, blurCoordinates[8]).rgb * 0.05;\n" +
                "\n" +
                "	gl_FragColor = vec4(sum,fragColor.a);\n" +
                "}";
    }
}
