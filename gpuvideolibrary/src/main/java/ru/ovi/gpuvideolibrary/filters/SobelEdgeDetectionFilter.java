package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/2/16.
 */
public class SobelEdgeDetectionFilter implements IGroupFilters {

    public Texture3x3SamplingFilter mTexture3x3SamplingFilter;

    public SobelEdgeDetectionFilter(final float size, final float outputWidth, final float outputHeight) {
        mTexture3x3SamplingFilter = new Texture3x3SamplingFilter(size, outputWidth, outputHeight) {
            @Override
            public String getFragmentShader() {
                return
                        "#extension GL_OES_EGL_image_external : require\n" +
                                "precision mediump float;\n" +
                                "\n" +
                                "varying vec2 vTextureCoord;\n" +
                                "varying vec2 leftTextureCoordinate;\n" +
                                "varying vec2 rightTextureCoordinate;\n" +
                                "\n" +
                                "varying vec2 topTextureCoordinate;\n" +
                                "varying vec2 topLeftTextureCoordinate;\n" +
                                "varying vec2 topRightTextureCoordinate;\n" +
                                "\n" +
                                "varying vec2 bottomTextureCoordinate;\n" +
                                "varying vec2 bottomLeftTextureCoordinate;\n" +
                                "varying vec2 bottomRightTextureCoordinate;\n" +
                                "\n" +
                                "uniform samplerExternalOES sTexture;\n" +

                                "\n" +
                                "void main()\n" +
                                "{\n" +
                                "    float bottomLeftIntensity = texture2D(sTexture, bottomLeftTextureCoordinate).r;\n" +
                                "    float topRightIntensity = texture2D(sTexture, topRightTextureCoordinate).r;\n" +
                                "    float topLeftIntensity = texture2D(sTexture, topLeftTextureCoordinate).r;\n" +
                                "    float bottomRightIntensity = texture2D(sTexture, bottomRightTextureCoordinate).r;\n" +
                                "    float leftIntensity = texture2D(sTexture, leftTextureCoordinate).r;\n" +
                                "    float rightIntensity = texture2D(sTexture, rightTextureCoordinate).r;\n" +
                                "    float bottomIntensity = texture2D(sTexture, bottomTextureCoordinate).r;\n" +
                                "    float topIntensity = texture2D(sTexture, topTextureCoordinate).r;\n" +
                                "    float h = -topLeftIntensity - 2.0 * topIntensity - topRightIntensity + bottomLeftIntensity + 2.0 * bottomIntensity + bottomRightIntensity;\n" +
                                "    float v = -bottomLeftIntensity - 2.0 * leftIntensity - topLeftIntensity + bottomRightIntensity + 2.0 * rightIntensity + topRightIntensity;\n" +
                                "\n" +
                                "    float mag = length(vec2(h, v));\n" +
                                "\n" +
                                "    gl_FragColor = vec4(vec3(mag), 1.0);\n" +
                                "}";
            }
        };
    }


    @Override
    public IFilter[] getFilters() {
        return new IFilter[]{
                mTexture3x3SamplingFilter,
        };
    }
}
