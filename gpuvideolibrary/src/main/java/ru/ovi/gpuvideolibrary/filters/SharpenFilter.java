package ru.ovi.gpuvideolibrary.filters;

/**
 * Created by ovi on 3/4/16.
 */
public class SharpenFilter extends BaseFilter {

    private float mSharpness;
    private float mImageWidthFactorLocation;
    private float mImageHeightFactorLocation;

    public SharpenFilter(final float sharpness, final float width, final float height) {
        mSharpness = sharpness;
        mImageWidthFactorLocation = 1.0f / width;
        mImageHeightFactorLocation = 1.0f / height;
    }

    @Override
    public String getVertexShader() {
        return "attribute vec4 aPosition;\n" +
                "attribute vec4 aTextureCoord;\n" +
                "uniform mat4 uMVPMatrix;\n" +
                "uniform mat4 uSTMatrix;\n" +

                "\n" +
                "float imageWidthFactor = " + floatToString(mImageWidthFactorLocation) + "; \n" +
                "float imageHeightFactor = " + floatToString(mImageHeightFactorLocation) + "; \n" +
                "float sharpness = " + floatToString(mSharpness) + ";\n" +
                "\n" +

                "varying vec2 vTextureCoord;\n" +
                "varying vec2 leftTextureCoordinate;\n" +
                "varying vec2 rightTextureCoordinate; \n" +
                "varying vec2 topTextureCoordinate;\n" +
                "varying vec2 bottomTextureCoordinate;\n" +
                "\n" +
                "varying float centerMultiplier;\n" +
                "varying float edgeMultiplier;\n" +
                "\n" +
                "void main()\n" +
                "{\n" +
                "    gl_Position = uMVPMatrix * aPosition;\n" +
                "    \n" +
                "    mediump vec2 widthStep = vec2(imageWidthFactor, 0.0);\n" +
                "    mediump vec2 heightStep = vec2(0.0, imageHeightFactor);\n" +
                "    \n" +
                "    vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n" +
                "    leftTextureCoordinate = vTextureCoord - widthStep;\n" +
                "    rightTextureCoordinate = vTextureCoord + widthStep;\n" +
                "    topTextureCoordinate = vTextureCoord + heightStep;     \n" +
                "    bottomTextureCoordinate = vTextureCoord - heightStep;\n" +
                "    \n" +
                "    centerMultiplier = 1.0 + 4.0 * sharpness;\n" +
                "    edgeMultiplier = sharpness;\n" +
                "}";
    }

    @Override
    public String getFragmentShader() {
        return "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +
                        "\n" +
                        "varying highp vec2 vTextureCoord;\n" +
                        "varying highp vec2 leftTextureCoordinate;\n" +
                        "varying highp vec2 rightTextureCoordinate; \n" +
                        "varying highp vec2 topTextureCoordinate;\n" +
                        "varying highp vec2 bottomTextureCoordinate;\n" +
                        "\n" +
                        "varying highp float centerMultiplier;\n" +
                        "varying highp float edgeMultiplier;\n" +
                        "\n" +
                        "uniform samplerExternalOES sTexture;\n" +
                        "\n" +
                        "void main()\n" +
                        "{\n" +
                        "    mediump vec3 textureColor = texture2D(sTexture, vTextureCoord).rgb;\n" +
                        "    mediump vec3 leftTextureColor = texture2D(sTexture, leftTextureCoordinate).rgb;\n" +
                        "    mediump vec3 rightTextureColor = texture2D(sTexture, rightTextureCoordinate).rgb;\n" +
                        "    mediump vec3 topTextureColor = texture2D(sTexture, topTextureCoordinate).rgb;\n" +
                        "    mediump vec3 bottomTextureColor = texture2D(sTexture, bottomTextureCoordinate).rgb;\n" +
                        "\n" +
                        "    gl_FragColor = vec4((textureColor * centerMultiplier - (leftTextureColor * edgeMultiplier + rightTextureColor * edgeMultiplier + topTextureColor * edgeMultiplier + bottomTextureColor * edgeMultiplier)), texture2D(sTexture, bottomTextureCoordinate).w);\n" +
                        "}";
    }
}
