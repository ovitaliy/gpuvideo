package ru.ovi.gpuvideolibrary;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import ru.ovi.gpuvideolibrary.filters.BaseFilter;
import ru.ovi.gpuvideolibrary.filters.IFilter;


/**
 * Created by ovi on 3/1/16.
 */
class TextureRender {
    private static final String TAG = "TextureRender";
    private static final int FLOAT_SIZE_BYTES = 4;
    private static final int TRIANGLE_VERTICES_DATA_STRIDE_BYTES = 5 * FLOAT_SIZE_BYTES;
    private static final int TRIANGLE_VERTICES_DATA_POS_OFFSET = 0;
    private static final int TRIANGLE_VERTICES_DATA_UV_OFFSET = 3;
    private final float[] mTriangleVerticesData = {
            // X, Y, Z, U, V
            -1.0f, -1.0f, 0, 0.f, 0.f,
            1.0f, -1.0f, 0, 1.f, 0.f,
            -1.0f, 1.0f, 0, 0.f, 1.f,
            1.0f, 1.0f, 0, 1.f, 1.f,
    };
    private FloatBuffer mTriangleVertices;

    private float[] mMVPMatrix = new float[16];
    private float[] mSTMatrix = new float[16];
    private int[] mPrograms;
    private int mTextureID = -123456;
    private int[] muMVPMatrixHandles;
    private int[] muSTMatrixHandles;
    private int[] maPositionHandles;
    private int[] maTextureHandles;

    private IFilter[] mIFilters;

    private int mFrameBuffers;

    public TextureRender() {
        mTriangleVertices = ByteBuffer.allocateDirect(mTriangleVerticesData.length * FLOAT_SIZE_BYTES).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTriangleVertices.put(mTriangleVerticesData).position(0);
        Matrix.setIdentityM(mSTMatrix, 0);
    }

    public int getTextureId() {
        return mTextureID;
    }

    int frames = 0;

    public void drawFrame(SurfaceTexture st) {
        // GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers);
        //GLES20.glClearColor(0, 0, 0, 0);


        for (int i = 0; i < mPrograms.length; i++) {
            st.getTransformMatrix(mSTMatrix);
            checkGlError("onDrawFrame start");

            GLES20.glUseProgram(mPrograms[i]);
            checkGlError("glUseProgram");

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID);

            mTriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
            GLES20.glVertexAttribPointer(maPositionHandles[i], 3, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
            checkGlError("glVertexAttribPointer maPosition");

            GLES20.glEnableVertexAttribArray(maPositionHandles[i]);
            checkGlError("glEnableVertexAttribArray maPositionHandle");

            mTriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
            GLES20.glVertexAttribPointer(maTextureHandles[i], 2, GLES20.GL_FLOAT, false, TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
            checkGlError("glVertexAttribPointer maTextureHandle");

            GLES20.glEnableVertexAttribArray(maTextureHandles[i]);
            checkGlError("glEnableVertexAttribArray maTextureHandle");

            Matrix.setIdentityM(mMVPMatrix, 0);
            GLES20.glUniformMatrix4fv(muMVPMatrixHandles[i], 1, false, mMVPMatrix, 0);
            checkGlError("glUniformMatrix4fv");

            GLES20.glUniformMatrix4fv(muSTMatrixHandles[i], 1, false, mSTMatrix, 0);
            checkGlError("glUniformMatrix4fv");

            mIFilters[i].onDrawArraysPre();

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
            checkGlError("glDrawArrays");

           /* GLES20.glDisableVertexAttribArray(maTextureHandles[i]);
            GLES20.glDisableVertexAttribArray(maPositionHandles[i]);*/

            //GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, 0);

            GLES20.glFinish();

        }

       /* if (frames < 10) {
            try {
                File f = new File(Environment.getExternalStorageDirectory(), "tt/" + frames++ + ".png");
                saveFrame(f.getAbsolutePath(), 480, 480);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }*/
    }

    /**
     * Initializes GL state.  Call this after the EGL surface has been created and made current.
     */
    public void surfaceCreated() {
        mPrograms = createProgram(new BaseFilter());
        if (mPrograms == null) {
            throw new RuntimeException("failed creating program");
        }

     /*   int[] frameBuffers = new int[1];
        GLES20.glGenFramebuffers(1, frameBuffers, 0);
        checkGlError("glGenFramebuffers");
        mFrameBuffers = frameBuffers[0];
*/

        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);
        mTextureID = textures[0];

        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID);
        checkGlError("glBindTexture mTextureID");

        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        checkGlError("glTexParameteri");

     /*   GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers);
        checkGlError("glBindFramebuffer");

        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID, 0);
        checkGlError("glFramebufferTexture2D");*/

      /*  GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);*/
        //GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
    }

    /**
     * Replaces the fragment shader.
     */
    public void changeFragmentShader(IFilter[] filters) {
        for (int program : mPrograms)
            GLES20.glDeleteProgram(program);

        mIFilters = filters;
        mPrograms = createProgram(filters);

        if (mPrograms == null) {
            throw new RuntimeException("failed creating program");
        }
    }

    private int loadShader(int shaderType, String source) {
        int shader = GLES20.glCreateShader(shaderType);
        checkGlError("glCreateShader type=" + shaderType);
        GLES20.glShaderSource(shader, source);
        GLES20.glCompileShader(shader);
        int[] compiled = new int[1];
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            Log.e(TAG, "Could not compile shader " + shaderType + ":");
            Log.e(TAG, " " + GLES20.glGetShaderInfoLog(shader));
            GLES20.glDeleteShader(shader);
            shader = 0;
        }
        return shader;
    }

    private int[] createProgram(IFilter... filters) {
        int n = filters.length;

        maPositionHandles = new int[n];
        maTextureHandles = new int[n];
        muMVPMatrixHandles = new int[n];
        muSTMatrixHandles = new int[n];

        int[] programs = new int[n];


        for (int i = 0; i < n; i++) {
            int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, filters[i].getVertexShader());
            if (vertexShader == 0) {
                return null;
            }

            int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, filters[i].getFragmentShader());
            if (pixelShader == 0) {
                return null;
            }

            int program = GLES20.glCreateProgram();

            programs[i] = program;

            checkGlError("glCreateProgram");
            if (program == 0) {
                Log.e(TAG, "Could not create program");
            }
            GLES20.glAttachShader(program, vertexShader);
            checkGlError("glAttachShader");
            GLES20.glAttachShader(program, pixelShader);
            checkGlError("glAttachShader");

            GLES20.glLinkProgram(program);
            int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] != GLES20.GL_TRUE) {
                Log.e(TAG, "Could not link program: ");
                Log.e(TAG, GLES20.glGetProgramInfoLog(program));
                GLES20.glDeleteProgram(program);

                return null;
            }

            filters[i].init(program);

            maPositionHandles[i] = glGetAttribLocation(program, "aPosition");
            maTextureHandles[i] = glGetAttribLocation(program, "aTextureCoord");

            muMVPMatrixHandles[i] = glGetUniformLocation(program, "uMVPMatrix");
            muSTMatrixHandles[i] = glGetUniformLocation(program, "uSTMatrix");
        }

        return programs;
    }

    public static void checkGlError(String op) {
        int error;
        if ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }

    /**
     * Saves the current frame to disk as a PNG image.  Frame starts from (0,0).
     * <p/>
     * Useful for debugging.
     */
    public static void saveFrame(String filename, int width, int height) {
        // glReadPixels gives us a ByteBuffer filled with what is essentially big-endian RGBA
        // data (i.e. a byte of red, followed by a byte of green...).  We need an int[] filled
        // with native-order ARGB data to feed to Bitmap.
        //
        // If we implement this as a series of buf.get() calls, we can spend 2.5 seconds just
        // copying data around for a 720p frame.  It's better to do a bulk get() and then
        // rearrange the data in memory.  (For comparison, the PNG compress takes about 500ms
        // for a trivial frame.)
        //
        // So... we set the ByteBuffer to little-endian, which should turn the bulk IntBuffer
        // get() into a straight memcpy on most Android devices.  Our ints will hold ABGR data.
        // Swapping B and R gives us ARGB.  We need about 30ms for the bulk get(), and another
        // 270ms for the color swap.
        //
        // Making this even more interesting is the upside-down nature of GL, which means we
        // may want to flip the image vertically here.
        ByteBuffer buf = ByteBuffer.allocateDirect(width * height * 4);
        buf.order(ByteOrder.LITTLE_ENDIAN);
        GLES20.glReadPixels(0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buf);
        buf.rewind();
        int pixelCount = width * height;
        int[] colors = new int[pixelCount];
        buf.asIntBuffer().get(colors);
        for (int i = 0; i < pixelCount; i++) {
            int c = colors[i];
            colors[i] = (c & 0xff00ff00) | ((c & 0x00ff0000) >> 16) | ((c & 0x000000ff) << 16);
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filename);
            Bitmap bmp = Bitmap.createBitmap(colors, width, height, Bitmap.Config.ARGB_8888);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, fos);
            bmp.recycle();
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to write file " + filename, ioe);
        } finally {
            try {
                if (fos != null) fos.close();
            } catch (IOException ioe2) {
                throw new RuntimeException("Failed to close file " + filename, ioe2);
            }
        }
        Log.d(TAG, "Saved " + width + "x" + height + " frame as '" + filename + "'");
    }

    private static int glGetAttribLocation(int program, String attribute) {
        int value = GLES20.glGetAttribLocation(program, attribute);
        checkGlError("glGetAttribLocation " + attribute);
        if (value == -1) {
            throw new RuntimeException("Could not get attrib location for " + attribute);
        }
        return value;
    }

    private static int glGetUniformLocation(int program, String attribute) {
        int value = GLES20.glGetUniformLocation(program, attribute);
        checkGlError("glGetAttribLocation " + attribute);
        if (value == -1) {
            throw new RuntimeException("Could not get uniform location for " + attribute);
        }
        return value;
    }
}
